#include "server/passiveTCP.h"
#include <stdio.h>
#include "util/utilities.h"
#include <sys/epoll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <string.h>
#include <pthread.h>
#include "util/eventqueue.h"
#include <stdlib.h>
#include "util/eventqueue.h"

#define MAXUSERS 4000 // SQLITE_MINIMUM_FILE_DESCRIPTOR=N !!!! ustaw na wiekszy niz max liczba uzytkownikow...
#define BUFSIZE 50
#define NUMTHREADS 4


pthread_mutex_t queueMutex;
pthread_cond_t emptyCond;
pthread_cond_t fullCond;

eventQueue kolejka;

void *doIt(void *arg);


struct userJob {
    char job[BUFSIZE];
    int totalRead;
};


void getRequest(int, int, struct userJob *);


void addClient(int serverSocket, int epollObject) {
    int clientSocket = acceptTCP(serverSocket);
    struct epoll_event newConnection;
    newConnection.events = EPOLLIN;
    newConnection.data.fd = clientSocket;
    int cret = epoll_ctl(epollObject, EPOLL_CTL_ADD, clientSocket, &newConnection);
    exitIfNegative(cret, "epoll_ctl could not add client descriptor!");
    fprintf(stderr, "jeblem accepta\n");
}


int main() {


    kolejka = create(10);

    // inicjalizuj mutexy i CV
    pthread_mutex_init(&queueMutex, NULL);
    pthread_cond_init(&emptyCond, NULL);
    pthread_cond_init(&fullCond, NULL);

    // zespawnuj potrzebne watki
    pthread_t threads[NUMTHREADS];
    int i;
    for (i = 0; i < NUMTHREADS; i++) {
        if (pthread_create(&threads[i], NULL, doIt, NULL)) {
            printf("blad przy tworzeniu watku");
            abort();
        }
    }


    int serverSocket = passiveTCP("5558", 30);
    exitIfNegative(serverSocket, "creating socket failed!");
    setNonBlocking(serverSocket);

    int epollObject = epoll_create1(0);
    exitIfNegative(epollObject, "epoll_create() failed!");


    struct epoll_event newConnection;
    struct epoll_event epollEvents[100000];

    newConnection.events = EPOLLIN;
    newConnection.data.fd = serverSocket;


    struct userJob userBuffers[MAXUSERS + 5]; // fds already taken: epoll, serversocket, stdin, stdout, stderr = 5


    int cret = epoll_ctl(epollObject, EPOLL_CTL_ADD, serverSocket, &newConnection);
    exitIfNegative(cret, "epoll_ctl could not add server descriptor!");


    int numEvents;
    int eventTypes;
    int eventSocket;


    for (; ;) {
        numEvents = epoll_wait(epollObject, epollEvents, 10000, 10000);

        exitIfNegative(numEvents, "epoll_wait failed!");

        if (numEvents == 0)
            fprintf(stderr, "No events in this timeout, housekeeeping...\n");

        else {
            int i;

            for (i = 0; i < numEvents; i++) {

                eventTypes = epollEvents[i].events;
                eventSocket = epollEvents[i].data.fd;

                if (eventTypes & EPOLLIN) {
                    if (eventSocket == serverSocket) {// nowe placzenie
                        addClient(eventSocket, epollObject);
                        userBuffers[eventSocket].totalRead = 0;
                        userBuffers[eventSocket].job[0] = '\0';
                    }
                    else
                        getRequest(eventSocket, epollObject, userBuffers);
                    }
                }
            }
        }
    }


void getRequest(int clientSocket, int epollObject, struct userJob *userBuffers) {
    char commandBuffer[BUFSIZE];
    commandBuffer[0] = '\0';
    int rc = recv(clientSocket, commandBuffer, (size_t) BUFSIZE, 0);

    if (rc <= 0) {
        fprintf(stderr, "Connection closed. \n");
        close(clientSocket);
        epoll_ctl(epollObject, EPOLL_CTL_DEL, clientSocket, NULL);
    }

    else {

        if ((userBuffers[clientSocket].totalRead + rc) <=
            BUFSIZE) // miesci sie, wiec dopisz, jak sie nie miesci nie rob nic
        {
            strcat(userBuffers[clientSocket].job, commandBuffer);
            userBuffers[clientSocket].totalRead += rc;
        }

        if (commandBuffer[rc - 1] == '\n') {    // koniec komendy, odeslij i wyzeruj
            userBuffers[clientSocket].job[userBuffers[clientSocket].totalRead] = '\0';
            send(clientSocket, userBuffers[clientSocket].job, userBuffers[clientSocket].totalRead, 0);

            pthread_mutex_lock(&queueMutex);  // zaloz lock na kolejke i czekaj az nie bedzie ona pelna
            while (isFull(kolejka))
                pthread_cond_wait(&fullCond, &queueMutex);

            push(kolejka, userBuffers[clientSocket].job);
            pthread_cond_signal(&emptyCond);  // sygnalizuj ze jest cos do zrobienia
            pthread_mutex_unlock(&queueMutex);
            // pierwsze polecenie bedzie zbugowane bo nigdzie nie zakanczam tego stringa '\0'
            userBuffers[clientSocket].totalRead = 0;
            userBuffers[clientSocket].job[0] = '\0';
            // zamarkuj jako completed request a potem return ze pelen zakonczony, potem w glownym watku
            // dodaj tego requesta do kolejki, no i wsio.
        }
    }
}


void *doIt(void *arg) {
    char *job;
    while (1) {
        pthread_mutex_lock(&queueMutex);  // zaloz lock na kolejke i czekaj az cos w niej bedzie
        while (isEmpty(kolejka))
            pthread_cond_wait(&emptyCond, &queueMutex);

        job = pop(kolejka);
        pthread_cond_signal(&fullCond);
        pthread_mutex_unlock(&queueMutex);
        printf("Jestem watkiem id %ld a moim zadaniem jest %s\n", (long) pthread_self(), job);
        free(job);
        sleep(2);

    }
}
