#ifndef PASSIVETCP_H
#define PASSIVETCP_H


int passiveTCP(char *service, int qlen);

int acceptTCP(int serverSocket);

void setNonBlocking(int socket);

#endif