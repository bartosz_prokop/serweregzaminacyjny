#ifndef UTILITIES_H
#define UTILITIES_H
#include <sys/resource.h>

rlim_t getDescriptorLimit();

void errorExit(char *msg);

void exitIfNegative(int num, char *msg);

#endif