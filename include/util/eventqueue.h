#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include <stdbool.h>

typedef struct eventQueue_type *eventQueue;


eventQueue create(int capacity);

void destroy(eventQueue q);

void makeEmpty(eventQueue q);

bool isEmpty(eventQueue q);

bool isFull(eventQueue q);

void push(eventQueue q, const char *job);

char *pop(eventQueue q); // u need to free memory pointed to by return value of pop!!!!!!

int getCapacity(eventQueue q);

int getCount(eventQueue q);

#endif
