#include "util/utilities.h"
#include <stdio.h>
#include <stdlib.h>

rlim_t getDescriptorLimit() {
    struct rlimit limits;
    getrlimit(RLIMIT_NOFILE, &limits);
    return limits.rlim_cur;
}

void errorExit(char *msg) {
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

void exitIfNegative(int num, char *msg) {
    if (num < 0)
        errorExit(msg);
}