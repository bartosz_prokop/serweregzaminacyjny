#include "server/passiveTCP.h"
#include <arpa/inet.h>
#include "server/passivesock.h"
#include "stdlib.h"
#include "stdio.h"
#include "fcntl.h"

int passiveTCP(char *service, int qlen) {
    return passivesock(service, "tcp", qlen);
}

int acceptTCP(int serverSocket) {
    int clientSocket;
    struct sockaddr_in clientAdr;
    unsigned int clientLen;

    clientLen = sizeof(clientAdr);

    if ((clientSocket = accept(serverSocket, (struct sockaddr *) (&clientAdr), &clientLen)) < 0)
        exit(EXIT_FAILURE);
    printf("Przetwarzam klienta %s\n",
           inet_ntoa(clientAdr.sin_addr));
    return clientSocket;
}

void setNonBlocking(int socket) {
    if (fcntl(socket, F_SETFL, O_NONBLOCK) < 0) {
        fprintf(stderr, "Nie moge ustawic socketu w tryb nieblokujacy.");
        exit(1);
    }
}
