#include "util/eventqueue.h"
#include <malloc.h>
#include <string.h>


struct node {
    char data[50];
    struct node *next;
};

struct eventQueue_type {
    struct node *front;
    struct node *rear;
    int capacity;
    int count;
};


eventQueue create(int capacity) {
    eventQueue newQueue = (eventQueue) malloc(sizeof(struct eventQueue_type));
    newQueue->front = NULL;
    newQueue->rear = newQueue->front;
    newQueue->count = 0;
    newQueue->capacity = capacity;
    return newQueue;
}

void destroy(eventQueue q) {
    makeEmpty(q);
    free(q);
}

void makeEmpty(eventQueue q) {
    while (q->front) {
        char *tmp = pop(q);
        free(tmp);
    }
}

bool isEmpty(eventQueue q) {
    return q->count == 0;
}

bool isFull(eventQueue q) {
    return q->count == q->capacity;
}

void push(eventQueue q, const char *job) {
    struct node *newEvent = (struct node *) malloc(sizeof(struct node));
    newEvent->next = NULL;
    strncpy(newEvent->data, job, 50);

    if (!q->front) {
        q->front = newEvent;
        q->rear = q->front;
    }

    else {
        q->rear->next = newEvent;
        q->rear = newEvent;
    }
    q->count++;

}

char *pop(eventQueue q) {
    if (q->front) {
        char *returnedJob = (char *) malloc(50);
        strncpy(returnedJob, q->front->data, 50);
        struct node *tmp = q->front;
        q->front = q->front->next;
        free(tmp);
        q->count--;
        return returnedJob;
    }
    return NULL;
}

int getCapacity(eventQueue q) {
    return q->capacity;
}

int getCount(eventQueue q) {
    return q->count;
};
