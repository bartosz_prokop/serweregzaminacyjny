#include "server/processClient.h"
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#define BUFWE 32


void processClient(int clientSocket) {
    char echoBufor[BUFWE];
    int otrzTekstDl;

    otrzTekstDl = recv(clientSocket, echoBufor, BUFWE, 0);
    if (otrzTekstDl < 0)
        exit(EXIT_FAILURE);

    while (otrzTekstDl > 0) {
        if (send(clientSocket, echoBufor, otrzTekstDl, 0)
            != otrzTekstDl)
            exit(EXIT_FAILURE);
        otrzTekstDl = recv(clientSocket, echoBufor, BUFWE, 0);
        if (otrzTekstDl < 0)
            exit(EXIT_FAILURE);
    }
    close(clientSocket);
}