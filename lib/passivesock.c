#include "server/passivesock.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>


u_short portbase = 0;


int passivesock(char *service, char *transport, int qlen) {
    struct servent *pse;

    struct protoent *ppe;
    struct sockaddr_in sin;


    int s, type;

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;

    if ((pse = getservbyname(service, transport)))
        sin.sin_port = htons(ntohs((u_short) pse->s_port) + portbase);
    else if ((sin.sin_port = htons((u_short) atoi(service))) == 0) {
        fprintf(stderr, "can't get \" %s \" service entry \n", service);
        exit(EXIT_FAILURE);
    }
    if ((ppe = getprotobyname(transport)) == 0) {
        fprintf(stderr, "can't get \" %s \" protocol entry \n", transport);
        exit(EXIT_FAILURE);
    }

    if (strcmp(transport, "udp") == 0)
        type = SOCK_DGRAM;
    else
        type = SOCK_STREAM;

    s = socket(PF_INET, type, ppe->p_proto);
    if (s < 0) {
        fprintf(stderr, "can't create socket");
        exit(EXIT_FAILURE);
    }

    if (bind(s, (struct sockaddr *) &sin, sizeof(sin)) < 0) {
        fprintf(stderr, "can't bind to socket to port");
        exit(EXIT_FAILURE);
    }
    if (type == SOCK_STREAM && listen(s, qlen) < 0) {
        fprintf(stderr, "can't listen)");
        exit(EXIT_FAILURE);
    }
    return s;
}

